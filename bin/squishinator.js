#!/usr/bin/env node


var fs			= require("fs");
var cheerio 	= require('cheerio');
var jsp			= require("uglify-js").parser;
var pro			= require("uglify-js").uglify;


const ERR_BADCLI		= 1;
const ERR_IOERROR		= 2;
const ERR_BADDIRECTIVE 	= 3;
const ERR_BADSECTION 	= 4;

const SQUISH_BEGIN	= "<!-- squishinator begin ";
const SQUISH_END	= "<!-- squishinator end ";

var destDir;
var sourceFname;
var baseDir;

var inlineMode;
var combineMode;
var minifyMode;

var combinedFname;
var thingsToDo = [];

function printUsage()
{
	console.log("node scriptinator.js -d {destination directory} {htmlFile}");
}


function parseCliArgs()
{
	var ret = false;

	if(process.argv.length < 5) {
		return ret;
	}

	var args = process.argv.slice(0);

	ret = args[0].indexOf("node") >= 0;

	args.shift();
	args.shift();

	ret = ret || args[0] == "-d";

	if(ret) {
		args.shift();
		destDir = args.shift();
		sourceFname = args.shift();

		var lastSlasPos = sourceFname.lastIndexOf("/");
		if(lastSlasPos >= 0) {
			baseDir = sourceFname.substr(0, lastSlasPos + 1);
			sourceFname = sourceFname.substr(lastSlasPos + 1)
		} else {
			baseDir = "./";
		}
	}

	return ret;
}


function validateCliArgs()
{
	var ret = true;

	if(destDir == sourceFname) {
		console.log("Source and Destination file names cannot be the same");
		ret = false
	}

	return ret;
}


//function getMinFname(fname)
//{
//	var baseName;
//	var ext;
//
//	var lastDotPos = fname.lastIndexOf(".");
//	if(lastDotPos >= 0) {
//		baseName = fname.substr(0, lastDotPos);
//		ext = fname.substr(lastDotPos);
//	} else {
//		baseName = fname;
//		ext = ""
//	}
//
//	var ret = baseName + ".min" + ext;
//
//	return ret;
//}


function minifyCss(css)
{
	var ret = [];

	css = css.replace(/\t/g," ");

	var cssArray = css.split("\n");

	for(var lineNo = 0; lineNo < cssArray.length; ++lineNo) {
		var line = cssArray[lineNo] || " ";
		ret.push(line.trim());
	}

	ret = ret.join(" ");

	do {
		var oldLen = ret.length;

		ret = ret.replace(/  /g," ");
	} while(ret.length < oldLen);

	return ret;
}


function minifyScript(script)
{
	var ast = jsp.parse(script); // parse code and get the initial AST
	ast = pro.ast_mangle(ast); // get a new AST with mangled names
	ast = pro.ast_squeeze(ast); // get an AST with compression optimizations
	var ret = pro.gen_code(ast); // compressed code here

	return ret;
}

function minify(data, type)
{
	var ret;

	if(type == "css") {
		ret = minifyCss(data);
	} else {
		ret = minifyScript(data);
	}

	return ret;
}


function getPath(fname)
{
	var ret = fname;
	var lastSlashPos = fname.lastIndexOf("/");

	if(lastSlashPos < 0) {
		ret = ""
	} else {
		ret = ret.substr(0, lastSlashPos);
	}

	return ret;
}


function mkDirs(baseDir, path)
{
	var subDirs = path.split("/");

	for(var subDirNo = 0; subDirNo < subDirs.length; ++subDirNo) {
		var dirParts = subDirs.slice(0, subDirNo + 1);
		var dir = baseDir + "/" + dirParts.join("/");

		var exists = fs.existsSync(dir);

		if(exists) {
			var stat = fs.statSync(dir);

			if(stat.isDirectory()) {
				continue;
			} else {
				console.log("Directory "
						+ dir
						+ " cannot be created, it already exists and is not a directory");
				process.exit(ERR_IOERROR)
			}
		}

		fs.mkdirSync(dir);
	}
}


function resetModes()
{
	inlineMode = false;
	combineMode = false;
	minifyMode = false;

	thingsToDo = [];
}


function isBeginningOfOperation(line)
{
	var ret = line.toLowerCase().indexOf(SQUISH_BEGIN) >= 0;
	return ret;
}


function isEndOfOperation(line)
{
	var ret = line.toLowerCase().indexOf(SQUISH_END) >= 0;
	return ret;
}


function parseDirectives(line)
{
	var tLine = line.toLowerCase();

	var openPos = line.indexOf("[");
	var closePos = line.indexOf("]");

	if(openPos >= 0) {
		if(closePos < openPos) {
			console.log("Missing closing bracket");
			process.exit(ERR_BADDIRECTIVE);
		}

		combinedFname = line.substring(openPos + 1, closePos);
		combinedFname = combinedFname || " ";
		combinedFname = combinedFname.trim();

		if(!combinedFname) {
			console.log("Missing filename");
			process.exit(ERR_BADDIRECTIVE);
		}
	}

	inlineMode = tLine.indexOf(" inline ") >= 0;
	combineMode =tLine.indexOf(" combine ") >= 0;
	minifyMode = tLine.indexOf(" minify ") >= 0;

	if(!inlineMode && (combineMode  && !combinedFname)) {
		console.log("directive needs filename, or to be inline");
		process.exit(ERR_BADDIRECTIVE);
	}
}


function isBlankLine(line)
{

	var ret = !line || line.length == 0;

	return ret;
}


function isTarget(line, target)
{
	line = line.toLowerCase();

	//if it isn't at the beginning of the line, we can't deal with it
	var ret = line.indexOf(target) == 0;

	return ret;
}


function isComment(line)
{
	var ret = isTarget(line, "<!--");

	return ret;
}


function isScript(line)
{
	var ret = isTarget(line, "<script");

	return ret;
}


function getScriptFname(line)
{
	var tag = $(line);
	var ret = tag.attr("src");

	return ret;
}



function isCss(line)
{
	line = line.toLowerCase();

	//if it isn't at the beginning of the line, we can't deal with it
	var ret = isTarget(line, "<link ");

	if(ret) {
		var tag = $(line);
		ret = tag.attr("rel") == "stylesheet";
	}

	return line;
}


function getCssFname(line)
{
	var tag = $(line);
	var ret = tag.attr("href");

	return ret;
}


function inSomeMode()
{
	var ret = inlineMode || combineMode || minifyMode;
	return ret;
}


function validateSection() {
	var sectionType = thingsToDo[0]["type"];

	for (var i = 1; i < thingsToDo.length; ++i) {
		if (thingsToDo[i]["type"] != sectionType) {
			console.log("Error, no mixing css and scripts in the same section");
			process.exit(ERR_BADSECTION);
		}
	}

	return sectionType
}


function getInlineCss(data, type)
{
	var ret = "<style type='text/css'>\n" + data + "\n</style>";

	return ret;
}


function getInlineScript(data, type)
{
	var ret = "<script type='text/javascript'>\n" + data + "</script>";

	return ret;
}


function getInlineTag(data, type)
{
	var ret;

	if(type == "css") {
		ret = getInlineCss(data)
	} else {
		ret = getInlineScript(data)

	}
	return ret;
}


function getExternalCss(fname)
{
	var ret = "<link rel=\"stylesheet\" href=\"" + fname + "\">";
	return ret;
}


function getExternalScript(fname)
{
	var ret = "<script type='text/javascript' src='" + fname + "'></script>";

	return ret;
}


function getExternalTag(fname, fileTime, type)
{
	var ret;

	var timeCode = fileTime.toString(36);
	fname = fname + "?cc=" + timeCode;

	if(type == "css") {
		ret = getExternalCss(fname)
	} else {
		ret = getExternalScript(fname)

	}
	return ret;
}


function getFileTime(fname) {
	var stats = fs.statSync(fname);

	var date = new Date(stats.mtime);

	var ret = date.getTime();

	return ret;
}


function squishinateSection(destFile)
{
	if(thingsToDo.length == 0) {
		return;
	}

	var combinedData = "";
	var combinedFileTime = 0;
	var inlineTag;
	var externalTag;

	var sectionType = thingsToDo[0]["type"];
	validateSection();

	for(var i = 0; i < thingsToDo.length; ++i) {
		var thingToDo = thingsToDo[i];

		var data = fs.readFileSync(baseDir + thingToDo.fname, "utf8");
		var fileTime = getFileTime(baseDir + thingToDo.fname);

		if(combineMode) {
			combinedData += data + "\n";

			if(fileTime > combinedFileTime) {
				combinedFileTime = fileTime;
			}
			continue;
		}

		if(minifyMode) {
			data = minify(data, thingToDo.type);
		}

		if(inlineMode) {
			inlineTag = getInlineTag(data, thingToDo.type);
			destFile.push(inlineTag);
		} else {
			var filePath = getPath(thingToDo.fname);
			mkDirs(destDir, filePath);

			fs.writeFileSync(destDir + "/" + thingToDo.fname, data, "utf8");

			externalTag = getExternalTag(thingToDo.fname, fileTime, thingToDo.type);
			destFile.push(externalTag);
		}
	}

	if(combineMode) {
		if(minifyMode) {
			combinedData = minify(combinedData, sectionType);
		}

		if(inlineMode) {
			inlineTag = getInlineTag(combinedData, sectionType);
			destFile.push(inlineTag);
		} else {
			var combinedPath = getPath(combinedFname);
			mkDirs(destDir, combinedPath);

			fs.writeFileSync(destDir + "/" + combinedFname, combinedData, "utf8");

			externalTag = getExternalTag(combinedFname, combinedFileTime, sectionType);
			destFile.push(externalTag);
		}
	}

	resetModes();
}


function beginSection(line) {
	thingsToDo = [];
	parseDirectives(line);
}


function handleSectionLine(line, destFile, lineNo) {
	if (isScript(line)) {
		var scriptFname = getScriptFname(line);
		thingsToDo.push({
			fname: scriptFname,
			type: "script"
		});
	} else if (isCss(line)) {
		var cssFname = getCssFname(line);
		thingsToDo.push({
			fname: cssFname,
			type: "css"
		});
	} else if (isBlankLine() || isComment()) {
		destFile.push(line);
	} else {
		console.log("Error at line " + (lineNo + 1)
				+ " only Scripts, CSS, single line comments, and blank"
				+ " lines allowed here.");
		process.exit(ERR_BADDIRECTIVE);
	}
}


function squishinateFile()
{
	resetModes();

	var sourceFile = fs.readFileSync(baseDir + sourceFname, "utf8");
	var destFile = [];

	if(!sourceFile) {
		console("Error reading " + sourceFile);
		process.exit(ERR_IOERROR);
	}

	sourceFile = sourceFile.toString();

	var sourceLines = sourceFile.split("\n");
	for(var lineNo = 0; lineNo < sourceLines.length; ++lineNo) {
		var line = sourceLines[lineNo];

		if(line) {
			line = line.trim();
		}

		if(isBeginningOfOperation(line)) {
			beginSection(line);
		} else if(isEndOfOperation(line)) {
			squishinateSection(destFile)
		} else if(inSomeMode()) {
			handleSectionLine(line, destFile, lineNo);
		} else {
			destFile.push(line);
		}
	}

	fs.writeFileSync(destDir + "/" + sourceFname, destFile.join("\n"));
}


function loadDomAndGo()
{
	var htmlSource = fs.readFileSync(baseDir + sourceFname, "utf8");
	$ = cheerio.load(htmlSource);
}


function main()
{
	if(parseCliArgs()) {
		if(validateCliArgs()) {
			loadDomAndGo();
		} else {
			printUsage();
			process.exit(ERR_BADCLI);
		}
	} else {
		printUsage();
		process.exit(ERR_BADCLI);
	}
}

main();

