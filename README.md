Squishinator
=============

Usage
-----

	squishinator -d {output directory} {html file}
	
Overview
---------

Squishinator is a utility to combine, minify, and/or inline JavaScript and CSS files in a single page application.  The utility also performs a light minification of the html itself (basically removing unneeded whitespace).

Squishinator directives take the form of html comments inside of the source.  For example:

	<!-- squishinator begin [script/dropbox-combined.js] combine minify -->
		<script type="text/javascript" src="script/oauth.js"></script>
		<script type="text/javascript" src="script/sha1.js"></script>
		<script type="text/javascript" src="script/dropbox.js"></script>
	<!-- squishinator end -->

Will combine the three javascript files into a single file (that will be named `dropbox-combined.js`, and placed in the `script` directory).

A directive begins with a comment that starts:

	<!-- squishinator begin 
	
An optional filename — used if you aren't inlining files — is placed inside of brackets ([]), and then some combination of the following three options may be added:

* `combine` — combines the files into a single new file.  If you aren't inlining you **must** provide a filename in brackets ([])
* `minify` — runs [UglifyJS](https://github.com/mishoo/UglifyJS) on the source to mangle the source and make it smaller
* `inline` — places the resulting file(s) directly into the source of the file

The comment is then closed as normal.  At the end of the block of files to act on place the ending directive (this is not optional)

	<!-- squishinator end -->
	
***NOTE:*** These directives can work on JavaScript and CSS files, but JavaScript and CSS *cannot* be mixed in the same directive.

Finally Squishinator will use cache busting URL to refer to the new file.  So if you just want a cache busting reference but don't want to minify the source you could do this:

	<!-- squishinator begin [script/bootstrap.min.js] combine -->
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<!-- squishinator end -->
